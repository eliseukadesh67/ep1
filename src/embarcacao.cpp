#include "../inc/embarcacao.hpp"


	Embarcacao::Embarcacao(){
		set_nome(" ");
		set_tam(0);
		set_direcao(" ");
		
	}
	
	Embarcacao::Embarcacao(string nome, int linha, int coluna, string direcao){
		set_nome(nome);
		set_linha(linha);
		set_coluna(coluna);
		set_direcao(direcao);
	}

	Embarcacao::~Embarcacao(){

	}

	string Embarcacao::get_nome(){
		return nome;
	}
	void Embarcacao::set_nome(string nome){
		this->nome = nome;
	}
	int Embarcacao::get_tam(){
		return tam;
	}
	void Embarcacao::set_tam(int tam){
		this-> tam = tam;
	}
	void Embarcacao::set_direcao(string direcao){
		this->direcao = direcao;
	}
	string Embarcacao::get_direcao(){
		return direcao;
	}
	void Embarcacao::set_linha(int linha){
		this-> linha = linha;
	}
	int Embarcacao::get_linha(){
		return linha;
	}
	void Embarcacao::set_coluna(int coluna){
		this->coluna = coluna;
	}
	int Embarcacao::get_coluna(){
		return coluna;
	}
	void Embarcacao::set_resist(int resist){
		this-> resist = resist;
	}
	int Embarcacao::get_resist(){
		return resist;
	}
	void Embarcacao::diminui_vida(){
		resist--;
	}
	void Embarcacao::set_vidas(int indice, int vida){

		vidas[indice] = vida;
	}
	int Embarcacao::get_vidas(int indice){
		return vidas[indice];
	}
	void Embarcacao::diminui_vidas(int indice){
		vidas[indice] = vidas[indice]-1;
	}
