#include "submarino.hpp"

using namespace std;

Submarino::Submarino(string nome, int linha, int coluna, string direcao){
	set_nome(nome);
	set_linha(linha);
	set_coluna(coluna);
	set_tam(2);
	set_direcao(direcao);
	set_resist(2);
	set_vidas(0, 2);
	set_vidas(1, 2);
}

Submarino::~Submarino(){

}