#include "../inc/porta_avioes.hpp"

using namespace std;

Porta_avioes::Porta_avioes(string nome, int linha, int coluna, string direcao){
	set_nome(nome);
	set_linha(linha);
	set_coluna(coluna);
	set_tam(4);
	set_direcao(direcao);
	set_resist(4);
	
}

Porta_avioes::~Porta_avioes(){

}
