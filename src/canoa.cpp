#include "../inc/canoa.hpp"

using namespace std;

Canoa::Canoa(string nome, int linha, int coluna, string direcao){
	set_nome(nome);
	set_linha(linha);
	set_coluna(coluna);
	set_tam(1);
	set_direcao(direcao);
	set_resist(1);
}

Canoa::~Canoa(){

}
