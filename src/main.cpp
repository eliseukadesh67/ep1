#include <iostream>
#include <string>
#include <cctype>
#include <fstream>
#include <vector>
#include <thread>
#include <chrono>
#include <iomanip>
#include "../inc/embarcacao.hpp"
#include "../inc/mapa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/porta_avioes.hpp"
#include "../inc/canoa.hpp"
#include <utility>
#include "../inc/ler_arquivo.hpp"
#define MAX_TAM 13
using namespace std;

struct infos info[24];

void agradecimentos();
void lets_play();
void menu();
void fim(string vencedor);

int main(){

	menu();

    return 0;
}
string escolha_arq(){
	system("clear");

	cout<< "Selecione o arquivo do mapa que deseja utilizar."<<endl;
	cout<< "1) map_1.txt"<<endl;
	cout<< "2) map_2.txt"<<endl;
	cout<< "3) map_3.txt"<<endl;

	int choice;

	cin>> choice;

	switch(choice){

		case 1:
			return "./doc/map_1.txt";
		break;

		case 2:
			return "./doc/map_2.txt";
		break;

		case 3:
			return "./doc/map_3.txt";
		break;

		default:
			return "Erro.";
		break;

	}
}
void menu(){
	system("clear");

	int option;

	cout<<"\t\t\t\t\t\tBATTLESHIP"<< endl;
	cout<<"\t\t\t\t\t\tFGA EDITION"<<endl;
	cout<<"-----------------------------------------------------------------------------------------------------------------------------------------------"<<endl;
	cout<< endl;
	cout<< "MENU: " << endl;
	cout<< "1) Iniciar o jogo"<<endl;
	cout<< "2) Sair do jogo"<<endl;
	cout << endl;

	cin>> option;

	switch(option){

		case 1 :
			lets_play();
		break;

		case 2:
			agradecimentos();
			exit(0);
		break;

		default:

		break;
	}
}
void regras_game(){
	int escolha;
		
	system("clear");
	cout<<"------------------------"<<endl;
	cout<< "Legendas do tabuleiro:"<<endl;
	cout<<"------------------------"<<endl;
	cout<< endl;
	cout<< "Posição não atacada: * "<<endl;
	cout<< "Posição destruida: # "<<endl;
	cout<< "Posição parcialmente destruida: + "<<endl;
	cout<< "Posição 'Água': ~ "<< endl;
	cout<< endl;
	cout<<"------------------------"<<endl;
	cout<< "Instruções:"<<endl;
	cout<<"------------------------"<<endl;
	cout<< "O jogador deverá inserir as coordenadas do ataque utilizando apenas letras para linhas, e numeros para colunas."<<endl;
	cout<<endl;
	cout<< "EXEMPLO:"<<endl;
	cout<<endl;
	cout<< "Linha = 'A'"<<endl;
	cout<< "Coluna = 2"<<endl;
	cout<< endl;
	cout<<"OBS) Se o jogador inserir uma posição ja atacada ele perderá a vez."<<endl;
	cout<< endl;
	cout<< "1) OK. Começar!"<< endl;
	cout<< endl;

	cin>> escolha;	
}
void lets_play(){

	int linha1, coluna1, linha2, coluna2;
	char linha2_, linha1_;
	string name1, name2, vencedor, file;
	bool game_over=false;

	regras_game();

	system("clear");

	cout<< "Insira o nome do jogador 1"<< endl;
	cin>> name1;

	system("clear");

	cout<< "Insira o nome do jogador 2"<< endl;
	cin >> name2;

	file = escolha_arq();

	Mapa * mapa1 = new Mapa();
	Mapa * mapa2 = new Mapa();

	mapa1->zera_map();
	mapa2->zera_map();

	ler_arquivo(info, file);

	vector<Embarcacao *> barcos1;
	vector<Embarcacao *> barcos2;	
	
	for(int i=0; i<6; i++){
		barcos1.push_back(new Canoa(info[i].nomes, info[i].linhas, info[i].colunas, info[i].direcoes));
	}
	for(int j=6; j<10; j++){
		barcos1.push_back(new Submarino(info[j].nomes, info[j].linhas, info[j].colunas, info[j].direcoes));
	}
	for(int k=10; k<12; k++){
		barcos1.push_back(new Porta_avioes(info[k].nomes, info[k].linhas, info[k].colunas, info[k].direcoes));
	}
	for(int l=12; l<18; l++){
		barcos2.push_back(new Canoa(info[l].nomes, info[l].linhas, info[l].colunas, info[l].direcoes));
	}
	for(int m=18;m<22;m++){
		barcos2.push_back(new Submarino(info[m].nomes, info[m].linhas, info[m].colunas, info[m].direcoes));
	}
	for(int n=22; n<24; n++){
		barcos2.push_back(new Porta_avioes(info[n].nomes, info[n].linhas, info[n].colunas, info[n].direcoes));
	}

	mapa1->zera_tabuleiro();
	mapa1->set_mapa(barcos1);
	mapa2->zera_tabuleiro();
	mapa2->set_mapa(barcos2);
	
	
	
	system("clear");


	while(game_over==false){
		
		do{
			system("clear");

			mapa2->mostra_tabuleiro(name2);

			cout<<"Vez de "<< name1 <<"."<<endl;
			cout<<endl;
			cout<<"Informe as cordenadas do seu ataque " << name1 <<":" <<endl;
		
			cin>> linha2_ >> coluna2;

			linha2_ = toupper(linha2_);

			linha2 = (linha2_- 64);

			if(linha2>13 or coluna2>13 or linha2<1 or coluna2<1){

				cout<< "Coordenada não valida!"<<endl;
				std::this_thread::sleep_for(std::chrono::seconds(3));	
			}
			else{
				break;
			}
		}while(linha2>13 or coluna2>13 or linha2<1 or coluna2<1);

		mapa2->ataque_mapa(linha2, coluna2, name2, barcos2);
		

		if(mapa1->get_quant_embarc() > mapa2->get_quant_embarc()){
			vencedor = name1;
		}
		else{
			vencedor = name2;
		}

		std::this_thread::sleep_for(std::chrono::seconds(3));

		if(mapa2->get_quant_embarc()==0 or linha2==14 or coluna2==14){
			game_over = true;
			mapa2->mostra_tabuleiro(name2);
			cout<<"Todas as embarcações inimigas foram destuidas!"<<endl;
			std::this_thread::sleep_for(std::chrono::seconds(3));
			continue;
		}


		do{
			mapa1->mostra_tabuleiro(name1);

			cout<<"Vez de "<< name2 <<"."<<endl;
			cout<<endl;
			cout<<"Informe as cordenadas do seu ataque " << name2 <<":" <<endl;

			cin>> linha1_ >> coluna1;

			linha1_ = toupper(linha1_);

			linha1 = (linha1_- 64);

			if(linha1>13 or coluna1>13 or linha1<1 or coluna1<1){

				cout<< "Coordenada não valida!"<<endl;
				std::this_thread::sleep_for(std::chrono::seconds(3));	
			}
		}while(linha1>13 or coluna1>13 or linha1<1 or coluna1<1);

		mapa1->ataque_mapa(linha1, coluna1, name1, barcos1);

		if(mapa1->get_quant_embarc() > mapa2->get_quant_embarc()){
			vencedor = name1;
		}
		else{
			vencedor = name2;
		}

		std::this_thread::sleep_for(std::chrono::seconds(3));

		
		if(mapa1->get_quant_embarc()==0){
			game_over = true;
			mapa1->mostra_tabuleiro(name1);
			cout<<"Todas as embarcações inimigas foram destuidas!"<<endl;
			std::this_thread::sleep_for(std::chrono::seconds(3));
			continue;
		}
	}
	for(Embarcacao *p : barcos1){

		delete p;
	}
	for(Embarcacao *b : barcos2){

		delete b;
	}

	delete mapa2;
	delete mapa1;

	system("clear");
	cout<< vencedor <<" venceu o jogo!"<<endl;
	cout<<"----------------------------"<<endl;
	std::this_thread::sleep_for(std::chrono::seconds(3));

	int choose;

	cout<<"Deseja jogar novamente?"<<endl;

	cout<<"1) Sim."<<endl;
	cout<<"2) Não. Desejo voltar ao menu."<<endl;

	cin >> choose;

	switch(choose){
		case 1:
			lets_play();
			break;
		case 2:
			menu();
		break;
		
		default:

		break;
	}	
}
void agradecimentos(){

	system("clear");
	
	cout<< "OBRIGADO POR JOGAR."<<endl;
	cout<< endl;

	cout<< "Agredimentos especiais a todos os monitores da disciplina de 'OO'."<<endl;
	cout<<endl;
	cout<<"E muito obrigado ao Prof. Renato Sampaio."<<endl;
}
