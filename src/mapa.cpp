#include "../inc/mapa.hpp"
#include "../inc/embarcacao.hpp"
#include <string>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>
#include <ctime>
#include <thread>
#include <chrono>
#define TAM_MAX 13

using namespace std;

Mapa::Mapa(){
}

Mapa::~Mapa(){
}

void Mapa::zera_tabuleiro(){
   	for(int a=0;a<13;a++){

    	for(int b=0; b<TAM_MAX;b++){
        	this->interface[a][b] = '*';
    	}
  	}
} 		
		


void Mapa::mostra_tabuleiro(string name){
	
	system("clear");

        cout << endl;
        // Gerar as colunas de 1até13
        cout<< ' ';
        cout<< "MAPA DE "<< name <<"\t\t"<<"Numero de embarcações a serem destruidas: "<<get_quant_embarc()<<endl;
        cout<<setw(56)<<setfill('-')<< "\n";
        for(int i=0;i<TAM_MAX;i++){
                cout <<setw(4)<<i+1;
        }
                cout<< endl;
                cout<< setw(56) << setfill('-') << "\n";



        //Gerar linhas com letras

        for(int j=0;j<TAM_MAX;j++){
                cout<< setw(1) << setfill('-')<< setiosflags(ios::right) <<(char)(j+65);

                        cout<< " |";

                        for(int k=0;k<TAM_MAX;k++){
                                cout << ' '<< this->interface[j][k]<<" |";
                        }

                        cout<<endl;
                        cout<< setw(56) << setfill('-') << "\n";
        }
}
void Mapa::ataque_tabuleiro(int linha, int coluna, string name, int result){

	if(result == 1){
		interface[linha-1][coluna-1] = '#';
		mostra_tabuleiro(name);
	}
	else if(result == 2){
		interface[linha-1][coluna-1] = '+';
		mostra_tabuleiro(name);
	}
	else{
		interface[linha-1][coluna-1] = '~';
		mostra_tabuleiro(name);
	}
}
void Mapa::zera_map(){

	for(int i=0;i<TAM_MAX;i++){
		

		for(int j=0;j<TAM_MAX;j++){
			
			
			this->map[i][j] = -1;
		}
	}	
}		

void Mapa::set_mapa(vector<Embarcacao *> embarc){
	int k=0;

	for(Embarcacao *i : embarc){

		string direc = i->get_direcao();
		int linha = i->get_linha();
		int coluna = i->get_coluna();

		if(i->get_nome()=="porta-avioes"){	

			if(direc.at(0) == 'd'){


				map[linha][coluna] = k;
				map[linha][coluna+1] = k;
				map[linha][coluna+2] = k;
				map[linha][coluna+3] = k;	
			}
		
			else if(direc.at(0) == 'e'){

				map[linha][coluna] = k;
				map[linha][coluna-1] = k;
				map[linha][coluna-2] = k;
				map[linha][coluna-3] = k;	
			}
			else if(direc.at(0) == 'c'){
				map[linha][coluna] = k;
				map[linha-1][coluna] = k;
				map[linha-2][coluna] = k;
				map[linha-3][coluna] = k;
			}
			else if(direc.at(0) == 'b'){

				map[linha][coluna] = k;
				map[linha+1][coluna] = k;
				map[linha+2][coluna] = k;
				map[linha+3][coluna] = k;
			}		

			k++;
		}
		else if(i->get_nome()=="submarino"){

			if(direc.at(0) == 'd'){

				map[linha][coluna] = k;
				map[linha][coluna+1] = k;
			}
		
			else if(direc.at(0) == 'e'){

				map[linha][coluna] = k;
				map[linha][coluna-1] = k;	
			}
			else if(direc.at(0) == 'c'){
				map[linha][coluna] = k;
				map[linha-1][coluna] = k;
				
			}
			else if(direc.at(0) == 'b'){

				map[linha][coluna] = k;
				map[linha+1][coluna] = k;
			
			}
			
			k++;
		}
		else{
			
			map[i->get_linha()][i->get_coluna()] = k;
			k++;
		}	
		
	}	
}

void Mapa::mostra_mapa(){
	for(int h=0;h<13;h++){
		for(int j=0;j<13;j++){
			
			cout<< this->map[h][j];			
		}

		cout<< endl;
	}
}
int Mapa::get_quant_embarc(){

	return quant_embarc;

}
void Mapa::retira_quant_embarc(){
	quant_embarc--;
}
int Mapa::get_mapa(int linha, int coluna){
	return map[linha-1][coluna-1];
}
void Mapa::ataque_mapa(int linha, int coluna, string name, vector<Embarcacao *> embarc){

	int pos = get_mapa(linha, coluna);

	bool destrui_first = false;

		if(pos==-1){
			ataque_tabuleiro(linha, coluna, name, 0);
			cout<< "Água!"<< endl;
		}
		else if(pos<-1){
			mostra_tabuleiro(name);
			cout<< "Você ja acertou esta posição"<< endl;
		}
		else{
		
			if(embarc[pos]->get_nome()=="canoa"){
				embarc.at(pos)->diminui_vida();
				map[linha-1][coluna-1] = -2;
				retira_quant_embarc();
				ataque_tabuleiro(linha, coluna, name, 1);
				cout<< "Destrui a canoa!"<< endl;
			}
			else if(embarc.at(pos)->get_nome()=="submarino"){

				if(embarc.at(pos)->get_vidas(0)>0){
					embarc.at(pos)->diminui_vidas(0);
					mostra_tabuleiro(name);
				}
				else{
					destrui_first = true;
					embarc.at(pos)->diminui_vidas(1);
					mostra_tabuleiro(name);
				}
				if(embarc.at(pos)->get_vidas(0)<=0 and embarc.at(pos)->get_vidas(1)<=0){
					
					map[linha-1][coluna-1] = -2;
					retira_quant_embarc();
					ataque_tabuleiro(linha, coluna, name, 1);
					cout<< "Destrui o submarino completamente!"<< endl;	
				}
				else if(destrui_first == true or embarc.at(pos)->get_vidas(0)>0){
					ataque_tabuleiro(linha, coluna, name, 2);
					cout<< "Atacou parte do submarino!"<< endl;
					cout<<"Falta um ataque para destrui-la."<< endl;
				}
				else{
					map[linha-1][coluna-1] = -2;
					ataque_tabuleiro(linha, coluna, name, 1);
					cout<<"Destrui parte do submarino."<< endl;
				}
			}
			else{

				if(ramdom()==0){
					mostra_tabuleiro(name);
					cout<<"Missil abatido! Seu ataque falhou."<<endl;
				}
				else{

					embarc.at(pos)->diminui_vida();
					map[linha-1][coluna-1] = -2;
				
					if(embarc.at(pos)->get_resist()>0){
						ataque_tabuleiro(linha, coluna, name, 1);
						cout<< "Destrui parte do porta-avioes."<< endl;
					}
					else{
						retira_quant_embarc();
						ataque_tabuleiro(linha, coluna, name, 1);
						cout<< "Destrui o porta-avioes completamente!"<< endl;
					}
				}
			}
		}
}
int Mapa::ramdom(){
	srand(time(NULL));
	int ram = rand()%(2);
	return ram;
}

