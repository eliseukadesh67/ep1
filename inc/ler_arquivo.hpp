#ifndef LER_ARQUIVO_HPP
#define LER_ARQUIVO_HPP
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <ctime>
#include<chrono>
#include<thread>
//Arquivo com funções para ler o arquivo de texto que contém o mapa.

using namespace std;

struct infos{
	int linhas, colunas;
	string nomes, direcoes;
};

int string_converter(string str){

	if(str[0]=='0'){
		return 0;
	}
	else if(str[1]=='0'){
		return 10;
	}
	else if(str[1]=='1'){
		return 11;
	}
	else if(str[1]=='2'){
		return 12;
	}
	else if(str[0]=='1'){
		return 1;
	}
	else if(str[0]=='2'){
		return 2;
	}
	else if(str[0]=='3'){
		return 3;
	}
	else if(str[0]=='4'){
		return 4;
	}
	else if(str[0]=='5'){
		return 5;
	}
	else if(str[0]=='6'){
		return 6;
	}
	else if(str[0]=='7'){
		return 7;
	}
	else if(str[0]=='8'){
		return 8;
	}
	else if(str[0]=='9'){
		return 9;
	}
	else{
		return -1;
	}
}

void ler_arquivo(struct infos info[24],string filename){
	int linha, coluna, cont=0, first, length;
	string nome, direcao, linha_nova, coluna_nova;
	vector <string> arq;


	fstream reader;
	reader.open(filename, fstream::in);

	//Checar erro
	if(reader.fail()){
		cout << "Erro em abrir o arquivo" << endl;
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	else{
		//Ler arquivo
		while(!reader.eof()){
			char file_linha[256];
			string copia;
			reader.getline(file_linha, 256);

				if(file_linha[0] == '#' or file_linha[0] == '\n'){
					reader.ignore(256, '\n');
				}
				else{

						copia = file_linha;

						if(copia.size()>=14){

							arq.push_back(copia);
						}
				}
		}		
	}			

				reader.close();

					while(cont<24){
						first=0;
						length = arq[cont].find(' ', first);
						linha_nova = arq[cont].substr(first, (length-first));

						first = length+1;
						length = arq[cont].find(' ', first);
						coluna_nova = arq[cont].substr((first), (length-first));
					
						first = length+1;
						length = arq[cont].find(' ', first);
						nome = arq[cont].substr((first), (length-first));

						first = length+1;
						length = arq[cont].find(' ', first);
						direcao = arq[cont].substr((first), (length-first));

						linha = string_converter(linha_nova);
						coluna = string_converter(coluna_nova);

						info[cont].linhas = linha;
						info[cont].colunas = coluna;
						info[cont].nomes = nome;
						info[cont].direcoes = direcao;

						cont++;
					}
}
#endif
