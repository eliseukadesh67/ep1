#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>
#include <vector>
using namespace std;

class Embarcacao{

private:

	string nome;
	int tam;
	int linha, coluna;
	int resist;
	string direcao;
	int vidas[2];



public:

		Embarcacao();
		Embarcacao(string nome, int linha, int coluna, string direcao);
		~Embarcacao();

		string get_nome();
		void set_nome(string nome);
		int get_tam();
		void set_tam(int tam);
        int get_linha();
        void set_linha(int linha);
        int get_coluna();
        void set_coluna(int coluna);
        string get_direcao();
        void set_direcao(string direcao);
        void diminui_vida();
        void set_resist(int resist);
		int get_resist();
		void set_vidas(int indice, int vida);
		int get_vidas(int indice);
		void diminui_vidas(int indice);

			
};

#endif
