#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <string>
#include "embarcacao.hpp"

using namespace std;

class Submarino : public Embarcacao{

	private:

	public: 
	Submarino(string nome, int linha, int coluna, string direcao);
	~Submarino();
};
#endif
