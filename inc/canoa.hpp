#ifndef CANOA_HPP
#define CANOA_HPP
#include <string>
#include "embarcacao.hpp"

using namespace std;

class Canoa : public Embarcacao{
	private:

	public:
		
	Canoa(string nome, int linha, int coluna, string direcao);
	~Canoa();
};

#endif
