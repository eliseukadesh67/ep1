#ifndef MAPA_HPP
#define MAPA_HPP
#define TAM_MAX 13
#include "../inc/embarcacao.hpp"
#include "../inc/submarino.hpp"
#include <string>
#include <vector>

using namespace std;

class Mapa{

	private:
	int map[TAM_MAX][TAM_MAX];
	char interface[TAM_MAX][TAM_MAX];
	int quant_embarc=12;

	public:

	Mapa();
	~Mapa();

	int get_quant_embarc();
	void retira_quant_embarc();
	int get_mapa(int linha, int coluna);
	void zera_tabuleiro();
	void mostra_tabuleiro(string name);
	void zera_map();
	void set_mapa(vector<Embarcacao *> embarc);
	void mostra_mapa();
	void ataque_tabuleiro(int linha, int coluna, string name, int result);
	void ataque_mapa(int linha, int coluna, string name, vector<Embarcacao *> embarc);
	bool zerou_mapa();
	int ramdom();
};

#endif
