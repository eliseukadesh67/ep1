#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include <string>
#include "embarcacao.hpp"

using namespace std;

class Porta_avioes : public Embarcacao{
	private:

	public:
	Porta_avioes(string nome, int linha, int coluna, string direcao);
	~Porta_avioes();

};


#endif
