# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

&nbsp;&nbsp;Link do repositório do meu projeto:

(https://gitlab.com/eliseukadesh67/ep1)

## Instruções


* Descrição do projeto:


&nbsp;&nbsp;1 - Início do programa.


O jogo se inicia no menu principal, onde o usuário tera a opção de jogar, ou de sair do 	programa.

Após o usuario escolher iniciar o jogo, o programa irá mostrar as instruções básicas do 	funcionamento do jogo, como: as legendas do mapa e as instruções de ataques.

Após isso sera solicitado o nome do primeiro jogador, e o nome do segundo jogador.

Tendo o nome dos jogadores, o programa ira solicitar que um dos usuários selecione o 		arquivo do mapa que ele deseja jogar, este mapa irá possuir as posições e coordenadas de 	ambos os jogadores.


&nbsp;&nbsp;2 - O jogo começa.


Quando o jogo realmente começa é mostrado na tela o mapa do oponente, e embaixo o nome do 	jogador que possui a vez, e na mesma linha solicitando o jogador as coordenadas de seu 		ataque.As coordenadas são a linha, e a coluna em que o jogador deseja atacar. 

As coordenadas são apenas válidas se respeitarem este formato:

Linha de 'a' até 'm'.

Coluna de 1 até 13.

As rodadas se alternam entre o primeiro e o segundo jogador até um dos dois tenham 			destruido todas as embarcações do oponente.


&nbsp;&nbsp;3 - Ataques.


Após o jogador atacar, ele automaticamente terá um feedback de seu ataque, o mapa do 		aponente aparecerá novamente com as posições que foram atacadas atualizadas, e uma 		    mensagem de resposta como por exemplo: "Você destrui uma canoa.", ou "Seu ataque foi 		desviado!".


&nbsp;&nbsp;4 - Fim de jogo.


Quando todas as embarcações inimigas são destruídas, uma mensgem é mostrada ao jogador, informando que todas as embarcações do seu oponente foram destruídas, e o jogo é finalizado.

Na tela de fim de jogo é mostrado na tela o nome do vencedor, e logo depois o programa pergunta ao usuário deseja jogar novamente, se o usuário escolher "sim" o jogo se inicia novamete, se escolher "não" ele retorna ao menu principal.


* Instruções de execução



&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

##Observações

&nbsp;&nbsp;Recomenda-se executar o jogo em tela cheia.
